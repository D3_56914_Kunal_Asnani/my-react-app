import { Link } from "react-router-dom"

const Signup = () => {
  return (
    <div className="form">
      <h1>Signup</h1>

      <div className="mb-3">
        <label htmlFor="" className="label-control">
          First Name
        </label>
        <input type="text" className="form-control" />
      </div>

      <div className="mb-3">
        <label htmlFor="" className="label-control">
          Last Name
        </label>
        <input type="text" className="form-control" />
      </div>

      <div className="mb-3">
        <label htmlFor="" className="label-control">
          Email Address
        </label>
        <input type="text" className="form-control" />
      </div>

      <div className="mb-3">
        <label htmlFor="" className="label-control">
          Password
        </label>
        <input type="password" className="form-control" />
      </div>

      <div className="mb-3">
        <label htmlFor="" className="label-control">
          Confirm Password
        </label>
        <input type="password" className="form-control" />
      </div>

      <div className="mb-3">
        <div>
          Already have an account? <Link to="/signin">Signin here.</Link>
        </div>
        <button className="btn btn-primary">Signup</button>
      </div>
    </div>
  )
}

export default Signup
