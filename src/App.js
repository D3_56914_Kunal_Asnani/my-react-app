import Signin from './pages/Signin'
import Signup from './pages/signup'
import {BrowserRouter,Route,Routes} from 'react-router-dom'

function App() {
  return (
    <div className="container">
   <BrowserRouter>
   <Routes>
   <Route path="/signin" element={<Signin />} />
   <Route path="/signup" element={<Signup />} />
   </Routes>
   
   </BrowserRouter>
    </div>
  )
}

export default App